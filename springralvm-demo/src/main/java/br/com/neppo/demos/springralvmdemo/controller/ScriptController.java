package br.com.neppo.demos.springralvmdemo.controller;

import br.com.neppo.demos.springralvmdemo.components.engines.javascript.JsEngineComponent;
import br.com.neppo.demos.springralvmdemo.data.transfer.ScriptObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/calc")
public class ScriptController {

    @Autowired
    private JsEngineComponent engineComponent;

    @PostMapping
    @ResponseBody
    public Map execute(@RequestBody ScriptObject o){
        return engineComponent.calculate(o.getScript());
    }

}
