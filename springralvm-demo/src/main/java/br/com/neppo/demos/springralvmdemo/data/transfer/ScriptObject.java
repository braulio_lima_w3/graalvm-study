package br.com.neppo.demos.springralvmdemo.data.transfer;

public class ScriptObject {
    private String script;

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }
}
