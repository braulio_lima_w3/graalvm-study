package br.com.neppo.demos.springralvmdemo.components.engines.javascript;

import br.com.neppo.demos.springralvmdemo.service.CalculatorService;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.HostAccess;
import org.graalvm.polyglot.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Map;

@Component
public class JsEngineComponent {

    private Context context = Context.newBuilder().allowHostAccess(HostAccess.ALL).build();
    private Value scope = null;
    private static final String LANG = "js";
    private CalculatorService calculatorService;

    @Autowired
    public JsEngineComponent(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
        scope = context.eval(LANG, "this");
        injectContext();
    }

    private void injectContext(){
        scope.putMember("m", this.calculatorService);
    }

    public Map calculate(String script){
        if(script == null || StringUtils.isEmpty(script)){
            throw new RuntimeException("no script to execute");
        }
        return context.eval(LANG, script).as(Map.class);
    }

}
