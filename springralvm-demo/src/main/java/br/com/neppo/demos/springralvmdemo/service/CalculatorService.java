package br.com.neppo.demos.springralvmdemo.service;

import org.springframework.stereotype.Service;

@Service
public class CalculatorService {

    public Double add(Double a, Double b){
        if(a == null || b == null){
            return null;
        }
        return a + b;
    }
    public Double sub(Double a, Double b){
        if(a == null || b == null){
            return null;
        }
        return a - b;
    }
    public Double mul(Double a, Double b){
        if(a == null || b == null){
            return null;
        }
        return a * b;
    }
    public Double div(Double a, Double b){
        if(a == null || b == null){
            return null;
        }
        return a / b;
    }
    public Double mod(Double a, Double b){
        if(a == null || b == null){
            return null;
        }
        return a % b;
    }
    public Double pow(Double a, Double b){
        if(a == null || b == null){
            return null;
        }
        return Math.pow(a, b);
    }
    public Double sqr(Double a){
        if(a == null){
            return null;
        }
        return Math.sqrt(a);
    }

}
