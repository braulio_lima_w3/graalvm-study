package br.com.neppo.demos.springralvmdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringralvmDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringralvmDemoApplication.class, args);
	}

}
