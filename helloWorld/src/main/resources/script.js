class C{
    constructor(){
        this.val = null;
    }
    printName(){
        console.log(this.val);
    }
    setVal(val){
        this.val = val;
    }
}

var val = new C();

var allvals = [
    'Nigeria',
    'Sudan',
    'Iceland',
    'South Africa',
    'French Polynesia',
    'Syria',
    'Monaco',
    'Curaçao',
    'Haiti',
    'Austria',
    'Philippines',
    'Burkina Faso',
    'Guinea-Bissau',
    'Ghana',
    'Ukraine',
]

function change(){
    console.log("changing vals");
    val.printName();
    val.setVal(allvals[Math.floor(Math.random() * allvals.length)])
    val.printName();
}


// this works as well
function fooas(name){
    var Fooas = Java.type('br.com.neppo.net.services.FooaService');
    if(Fooas != null){
        var outpu = (Fooas.get().coolStory(name));
        console.log(outpu);
        return outpu;
    }
    return "";
}


this