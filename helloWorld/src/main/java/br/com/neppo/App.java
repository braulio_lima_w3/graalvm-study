package br.com.neppo;

import br.com.neppo.utils.FileLoader;
import br.com.neppo.utils.log.Log;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Source;
import org.graalvm.polyglot.Value;

import java.io.*;
import java.util.List;
import java.util.Map;

/**
 * Hello world!
 *
 */
public class App 
{
    private static final Log log = Log.GetFor(App.class);

    private static Context context = Context.newBuilder().allowAllAccess(true).build();

    public static void main( String[] args )
    {
        log.info("Starting to process script");
        InputStream stream  = null;
        try{

            log.info("Loading script from file");
            stream = ClassLoader.getSystemResourceAsStream("script.js");
            String baseScript = FileLoader.toString(stream);
            stream.close();

            log.info("Making source");
            Source source = Source.newBuilder("js", baseScript, "first")
                    .build();

            log.info("Evaluating context");
            Value v = context.eval(source);

            context.eval("js", "var a = 100;");
            context.eval("js", "var bou = { hello : 'world' };");

            v.getMemberKeys().forEach(System.out::println);
            Map map = v.getMember("bou").as(Map.class);
//            Value v = context.eval(source);
//            for (int i = 0; i < 10; i++) {
//                v.invokeMember("change");
//                System.out.println();
//            }
//            v.invokeMember("fooas", "Steve");
//
//            Value val = v.getMember("allvals");
//
//            for(Object s : val.as(List.class)){
//                log.info(s.toString());
//            }
//
//            context.eval("js", "fooas('Drake')").asString();

            log.info("breakpoint");

        }
        catch (Throwable t){
            log.warn(t);
        }

        log.info("Done");
    }

}
