package br.com.neppo.net.interfaces;

import br.com.neppo.jlibs.feign.FeignClient;
import br.com.neppo.net.model.FooasMessage;
import feign.Headers;
import feign.Param;
import feign.RequestLine;

public interface Fooas extends FeignClient {

    @RequestLine("GET /bm/{who}/{from}")
    @Headers({"Accept: application/json", "User-Agent: Mozilla/5.0"})
    FooasMessage bravo(@Param("who") String who, @Param("from") String from);

    @RequestLine("GET /bday/{who}/{from}")
    @Headers({"Accept: application/json", "User-Agent: Mozilla/5.0"})
    FooasMessage birthday(@Param("who") String who, @Param("from") String from);

    @RequestLine("GET /cool/{from}")
    @Headers({"Accept: application/json", "User-Agent: Mozilla/5.0"})
    FooasMessage coolStory(@Param("from") String from);

}
