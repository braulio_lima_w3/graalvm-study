package br.com.neppo.net.services;

import br.com.neppo.jlibs.feign.FeignBuilder;
import br.com.neppo.net.interfaces.Fooas;
import org.apache.commons.lang3.StringUtils;

public class FooaService {

    public static final String SERVICE_LOCATION = "http://www.foaas.com/";

    private FooaService(){}
    private Fooas fooas;

    private static FooaService service;
    public static FooaService get(){
        if(service == null){
            service = new FooaService();
            service.fooas = FeignBuilder.Make(SERVICE_LOCATION, Fooas.class)
                    .jackson(true)
                    .build();
        }
        return service;
    }

    public String coolStory(String from){
        if(StringUtils.isEmpty(from)) from = "";
        return fooas.coolStory(from).toString();
    }

    public String happyBirthday(String who, String from){
        if(StringUtils.isEmpty(who)) who = "";
        if(StringUtils.isEmpty(from)) from = "";
        return fooas.birthday(who, from).toString();
    }

    public String bravo(String who, String from){
        if(StringUtils.isEmpty(who)) who = "";
        if(StringUtils.isEmpty(from)) from = "";
        return fooas.bravo(who, from).toString();
    }
}
