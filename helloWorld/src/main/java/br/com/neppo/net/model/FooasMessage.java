package br.com.neppo.net.model;

public class FooasMessage {
    private String message;
    private String subtitle;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    @Override
    public String toString() {
        return String.format("%s %s",
                message == null ? "nuttin" : message,
                subtitle == null ? "- what?" : subtitle);
    }
}
