package br.com.neppo.utils;

import br.com.neppo.utils.log.Log;

import java.io.*;

public class FileLoader {
    private FileLoader(){}
    private static final Log log = Log.GetFor(FileLoader.class);

    public static InputStream loadFile(String name){
        return loadFile(new File(name));
    }

    public static InputStream loadFile(File f){
        if(f == null){
            throw new RuntimeException("File is null");
        }
        if(!f.exists() || !f.isFile() ){
            throw new RuntimeException("File does not exist");
        }
        try{
            return new FileInputStream(f);
        }
        catch (Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public static String toString(InputStream stream){
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
            StringBuilder builder = new StringBuilder();
            String line = reader.readLine();

            while (line != null) {
                builder.append(line).append('\n');
                line = reader.readLine();
            }

            return builder.toString();
        }
        catch (Exception e){
            throw new RuntimeException(e.getMessage());
        }
    }

}
