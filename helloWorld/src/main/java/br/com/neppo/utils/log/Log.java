package br.com.neppo.utils.log;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Log {
    private Log(){}

    public static enum Level {INFO, WARN, DANGER, FATAL}
    private String classname = "";
    private static String format="[%s][%s][%s] %s";

    public static Log GetFor(Class clazz){
        if(clazz == null){
            throw new RuntimeException("Empty class for log");
        }
        Log log = new Log();
        log.classname = clazz.getSimpleName();
        return log;
    }

    private static SimpleDateFormat DATE_FORMAT= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");


    public void log(Level level, String message) {
        log(level, message, null);
    }
    public void log(Level level, Throwable t){
        if(t == null){
            throw new RuntimeException("Throwable cannot be empty");
        }
        log(level, t.getMessage(), t);
    }
    public void log(Level level, String message, Throwable t){
        if(level == null){
            throw new RuntimeException("Empty logging information");
        }
        if(message == null){
            message = "";
        }

        System.out.println(String.format(format, DATE_FORMAT.format(new Date()), classname, level.name(), message ));

        if(t != null){
            t.printStackTrace();
        }
    }

    public void info(String message){
        log(Level.INFO, message);
    }

    public void warn(String message){
        log(Level.WARN, message);
    }
    public void warn(Throwable t){
        log(Level.WARN, t);
    }
    public void warn(String message, Throwable t){
        log(Level.WARN, message, t);
    }

    public void error(String message){
        log(Level.DANGER, message);
    }
    public void error(Throwable t){
        log(Level.DANGER, t);
    }
    public void error(String message, Throwable t){
        log(Level.DANGER, message, t);
    }

    public void critical(String message){
        log(Level.FATAL, message);
    }
    public void critical(Throwable t){
        log(Level.FATAL, t);
    }
    public void critical(String message, Throwable t){
        log(Level.FATAL, message, t);
    }
}
