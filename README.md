# Intro

To install GraalVM, follow their instructions over at their [website](http://www.graalvm.org).
Should be a 300 to 400 mb download depending on the version you choose to download.
The enterprise version inflates up to 1.1gb after the tarball file is extracted.

This `README` assumes you have both `JAVA_HOME` and `PATH` correctly configured.

## Just a small note...

If GraalVM is installed correctly and your java is set as something else (`OpenJDK`, `OracleJDK`, etc.)
Then follow these instructions:

```sh
# this will add graalvm to the list of alternatives you can set
sudo update-alternatives --install /usr/bin/java java /path/to/java 1000

# this will set graalvm's java as default
sudo update-alternatives --set /path/to/java java
```

This should work.
If you're using another sdk's java program, then you might run into some `class not found` type of errors.

That is all....

# Some demoing with graalvm

to run `test.js`

```sh
js test.js
```

to run `HelloWorld.java`

```sh
javac HelloWorld.java
java HelloWorld
```

# the HelloWorld project

Simple to get it up and going.

```sh
mvn clean package
java -cp target/helloWorld-0.0.1-SNAPSHOT.jar br.com.neppo.App
```

# spring boot

get it running like this:

```sh
mvn clean package
mvn spring-boot:run
```

## Examples of some requests you can make with the project:

`m` refers to a spring boot service which was injected into a component.

```
POST http://localhost:8091/calc HTTP/1.1
Content-Type: application/json

{
    "script" : "(function(){answer=0.0;answer+=m.pow(2.0,3.0);answer+=m.mul(answer,3.1);answer+=m.div(answer,1.5);answer+=m.sub(answer,1.1);answer+=m.add(answer,4.4);return{answer:answer}})()"
}
```

The previous will return:

```json
{
    "answer": 220.86666666666665
}
```

Just listing the elements in the `m` object, which is a spring boot service.

```
POST http://localhost:8091/calc HTTP/1.1
Content-Type: application/json

{
    "script" : "(function(){ return { m : Object.keys(m) } })()"
}
```

The previous will return:

```json
{
    "m": {
        "0": "mod",
        "1": "div",
        "2": "mul",
        "3": "sqr",
        "4": "sub",
        "5": "add",
        "6": "pow"
    }
}
```

However, as one can see, a js array `[]` doesn't seamlessly translate into a java `List<?>`.
This effect can be achieved in java with `value.as(List)` or `value.as(?[].class)`.
If you don't want to convert between the two, declare the java array in javascript:

```js
// executing: js --jvm

const ArrayList = Java.type('java.util.ArrayList');
var list = new ArrayList();
list.add(1);
list.add(2);
// ...
list.forEach(console.log);
```